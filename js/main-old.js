//Кнопка со всплывающей подсказкой
(function() {
	//Получает все кнопки-подсказки
	var buttonsHelp = document.getElementsByClassName("button-info");

	[].forEach.call(buttonsHelp, function(button) {
		button.onmouseover = function(event) {
			var message = event.currentTarget.getElementsByClassName('button-info__text-window')[0];
			message.classList.add("button-info__text-window_visible-block");
			setTimeout(function(){
				message.classList.add("button-info__text-window_visible-opacity");
			}, 50);
		};
		button.onmouseout = function(event) {
			var message = event.currentTarget.getElementsByClassName('button-info__text-window')[0];
			message.classList.remove("button-info__text-window_visible-opacity");
			setTimeout(function(){
				message.classList.remove("button-info__text-window_visible-block");
			}, 300);
		};
	});
}());

//select
(function() {

	//Генерирование списка
	var dates = document.getElementsByClassName("form-block__date");

	[].forEach.call(dates, function(date) {
		var day = date.getElementsByClassName("form-block__day")[0];
		var dayList = day.getElementsByClassName("select__list")[0];
		var month = date.getElementsByClassName("form-block__month")[0];
		var monthList = month.getElementsByClassName("select__list")[0];
		var year = date.getElementsByClassName("form-block__year")[0];
		var yearList = year.getElementsByClassName("select__list")[0];

		createSelectNumberItems(dayList, 1, 31);
		var months = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
		createSelectItems(monthList, months);
		createSelectNumberItemsReverse(yearList, 1901, 2002);
	});

		function createSelectNumberItems(directory, startNumber, endNumber) {
			for (; startNumber <= endNumber; startNumber++) {
				var li = document.createElement("li");
				li.className = "select__item";
				li.innerHTML = startNumber;
				directory.appendChild(li);
			}
		}

		function createSelectNumberItemsReverse(directory, startNumber, endNumber) {
			for (; startNumber <= endNumber; endNumber--) {
				var li = document.createElement("li");
				li.className = "select__item";
				li.innerHTML = endNumber;
				directory.appendChild(li);
			}
		}

		function createSelectItems(directory, contentTag) {
			for (var i = 0; i < contentTag.length; i++) {
				var li = document.createElement("li");
				li.className = "select__item";
				li.innerHTML = contentTag[i];
				directory.appendChild(li);
			}
		}

	//Открытие и закрытие списка select
	var selects = document.getElementsByClassName("select");
	[].forEach.call(selects, function(select) {
		select.onclick = function(event) {
			select.lastElementChild.classList.toggle("js-select__list_hidden");
		}
	});

	//Изменение value
	var selects = document.getElementsByClassName("select");
	changeValue(selects[0]);
	changeValueMonth(selects[1]);
	changeValue(selects[2]);

	function changeValue(select) {
		var select__items = select.getElementsByClassName("select__item");
		[].forEach.call(select__items, function(select__item) {
			select__item.onclick = function(event) {
				var value = event.currentTarget.innerHTML;
				var inputBlock = event.currentTarget.parentNode.parentNode.firstElementChild.firstElementChild;
				inputBlock.innerHTML = value;
				if (event.currentTarget.parentNode.firstElementChild == event.currentTarget) {
					inputBlock.classList.add("select__value_default");
				} else {
					inputBlock.classList.remove("select__value_default");
				}
			}
		});
	}

	function changeValueMonth(select) {
		var select__items = select.getElementsByClassName("select__item");
		[].forEach.call(select__items, function(select__item) {
			select__item.onclick = function(event) {
				var months = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
				var dayList = event.currentTarget.parentNode.parentNode.parentNode.firstElementChild.getElementsByClassName("select__list")[0];
				var value = event.currentTarget.innerHTML;
				var inputBlock = event.currentTarget.parentNode.parentNode.firstElementChild.firstElementChild;
				inputBlock.innerHTML = value;
				if (event.currentTarget.parentNode.firstElementChild == event.currentTarget) {
					inputBlock.classList.add("select__value_default");
				} else {
					inputBlock.classList.remove("select__value_default");
				}
				for (var i = 0; i < months.length; i++) {
					if (months[i] == event.currentTarget.innerHTML) break;
				}
				i++;
				if (i == 2) {
					while (dayList.children.length - 1 > 28) {
						dayList.removeChild(dayList.lastElementChild);
					}
				} else if (i%2 == 0) {
					if (dayList.children.length - 1 > 30) {
						while (dayList.children.length - 1 > 30) {
							dayList.removeChild(dayList.lastElementChild);
						}
					}
					else {
						createSelectNumberItems(dayList, +dayList.lastElementChild.innerHTML + 1, 30);
					}
				} else {
					if (dayList.children.length - 1 > 31) {
						while (dayList.children.length - 1 > 30) {
							dayList.removeChild(dayList.lastElementChild);
						}
					}
					else {
						createSelectNumberItems(dayList, +dayList.lastElementChild.innerHTML + 1, 31);
					}
				}
			}
		});
	}
}());

//Валидация полей и отправка на сервер
(function(){
	var submitEnter = document.forms["form-enter"];
	var submitReg = document.forms["form-reg"];

	submitEnter.onsubmit = function(event) {
		var login = event.currentTarget.elements["login"].value;
		var password = event.currentTarget.elements["password"].value;

		if (login == "") {
			focusInput(login);
			return false;
		}
		if (password == "") {
			focusInput(password);
			return false;
		}

		var queryAuthoriz = new XMLHttpRequest();
		queryAuthoriz.open("POST", "enter.php", true);
		var query = "login=" + login + "&password=" + password;
		queryAuthoriz.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		queryAuthoriz.send(query);
		queryAuthoriz.onreadystatechange = function() {
			if (queryAuthoriz.readyState == 4) {
				if(queryAuthoriz.status == 200) {
					alert(queryAuthoriz.responseText);
				}
			}
		};

		return false;
	}

	submitReg.onsubmit = function(event) {
		var firstName = event.currentTarget.elements["first-name"];
		var lastName = event.currentTarget.elements["last-name"];
		var selects = event.currentTarget.getElementsByClassName("select");
		var sex = event.currentTarget.elements["sex"];
		var MESSAGE_ERROR_NAME = "<b>Пожалуйста, укажите Ваше имя и фамилию</b>.<br>Чтобы облегчить общение и поиск друзей, у нас приняты настоящие имена и фамилии.";
		var MESSAGE_ERROR_BIRTHDAY = "<b>Пожалуйста, укажите дату рождения.</b><br>Вы сможете настроить, кто видит эту информацию.";
		var MESSAGE_ERROR_SEX = "Пожалуйста, укажите пол.";

		if (firstName.value == "") {
			focusInput(firstName);
			openMessageError(MESSAGE_ERROR_NAME, "error-message_first-name");
			return false;
		}
		if (lastName.value == "") {
			focusInput(lastName);
			openMessageError(MESSAGE_ERROR_NAME, "error-message_last-name");
			return false;
		}

		var isValid = true;
		for (i = 0; i < selects.length; i++) {
			var value = selects[i].firstElementChild.firstElementChild.innerHTML;
			var defaultValue = selects[i].lastElementChild.firstElementChild.innerHTML;
			if (value == defaultValue) {
				isValid = false;
			}
		}

		if (!isValid) {
			[].forEach.call(selects, function(select) {
				var value = select.firstElementChild.firstElementChild;
				focusInput(value);
			});
			openMessageError(MESSAGE_ERROR_BIRTHDAY, "error-message_birthday");
			return false;
		}

		if (sex.value == "") {
			var hidden = event.currentTarget.getElementsByClassName("sex-block")[0];
			hidden.classList.remove("sex-block-hidden");
			openMessageError(MESSAGE_ERROR_SEX, "error-message_sex");
			return false;
		}

		var dayValue = selects[0].firstElementChild.firstElementChild.innerHTML;
		var monthValue = selects[1].firstElementChild.firstElementChild.innerHTML;
		var yearValue = selects[2].firstElementChild.firstElementChild.innerHTML;

		var queryReg = new XMLHttpRequest();
		queryReg.open("POST", "reg.php", true);
		var query = "firstName=" + firstName.value + "&lastName=" + lastName.value + "&day=" + dayValue + "&month=" + monthValue + "&year=" + yearValue + "&sex=" + sex.value;
		queryReg.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		queryReg.send(query);
		queryReg.onreadystatechange = function() {
			if (queryReg.readyState == 4) {
				if(queryReg.status == 200) {
					alert(queryReg.responseText);
				}
			}
		};


		return false;
	}

	function focusInput(input) {
		input.classList.add("form-block__input_error");
		setTimeout(function() {
			input.classList.remove("form-block__input_error");
		}, 1000);
	}

	function openMessageError(text, classError) {
		var blockError = document.getElementsByClassName("error-message")[0];
		blockError.classList.add(classError);
		blockError.innerHTML = text;
	}

}());