//всплытие подсказки
(function() {
	var buttonsHelp = document.getElementsByClassName("button-info"); //Получает все кнопки-подсказки
	[].forEach.call(buttonsHelp, function(button) {
		button.onmouseover = function(event) {
			var message = event.currentTarget.getElementsByClassName('button-info__text-window')[0];
			message.classList.add("button-info__text-window_visible-block");
			setTimeout(function(){
				message.classList.add("button-info__text-window_visible-opacity");
			}, 50);
		};
		button.onmouseout = function(event) {
			var message = event.currentTarget.getElementsByClassName('button-info__text-window')[0];
			message.classList.remove("button-info__text-window_visible-opacity");
			setTimeout(function(){
				message.classList.remove("button-info__text-window_visible-block");
			}, 300);
		};
	});
})();

/**
 * Главная функция, содержит следующие функции:
 * <ul><li>Управление select</li>
 * <li>Валидация полей</li>
 * <li>Отправка на сервер</li></ul>
 */
(function() {

	//Управление select

	var day = new Select("js-select__day");
	day.createList("select__item", 1, 31);

	var month = new Select("js-select__month");
	var months = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август",
	"Сентябрь","Октябрь","Ноябрь","Декабрь"];
	month.createList("select__item", months);

	var year = new Select("js-select__year");
	year.createList("select__item", 2002, 1902);

	//Измениние количества дней от месяца
	for (var i = 1; i < month.getList().children.length; i++) {
		(month.getList().children)[i].addEventListener("click", dayDependMonth);
	}

	//Вспомогательные функции и классы.
	/**
	 * Класс осуществляет работу select
	 * @param {string} classNameSelect название класса, который принадлежит корневому узлу select
	 */
	function Select(classNameSelect) {
		//Получить нужные элементы.
			//Корневой узел select.
			var select = document.getElementsByClassName(classNameSelect)[0];
			//Выбранный элемент списка.
			var value = select.firstElementChild.firstElementChild.innerHTML;
			//Список значений.
			var list = select.lastElementChild;

		//Изменение value дефултного значения
		list.firstElementChild.onclick = alterValue;
		/**
		 * Изменение value взависимости от выбора элемента из списка.
		 */
		function alterValue(event) {
			var value = event.currentTarget.innerHTML;
			var inputBlock = event.currentTarget.parentNode.parentNode.firstElementChild.firstElementChild;
			inputBlock.innerHTML = value;
			if (event.currentTarget.parentNode.firstElementChild == event.currentTarget) {
				inputBlock.classList.add("select__value_default");
			} else {
				inputBlock.classList.remove("select__value_default");
			}
		}

		/**
		 * Добовляет в конец списка новый элемент.
		 * @param {string} classItem класс элемента
		 * @param {string} valueItem значение элемента.
		 * @return {HTML} созданный элемент списка.
		 */
		this.addItemEndList = function(classItem, valueItem) {
			var itemList = document.createElement("li");
			itemList.className = classItem;
			itemList.innerHTML = valueItem;
			list.appendChild(itemList);
			itemList.onclick = alterValue;
			return itemList;
		}

		/**
		 * Добовляет в конец списка новый элемент.
		 * @param {string} classItem класс элемента
		 * @param {string} valueItem значение элемента.
		 */
		this.deleteItemEndList = function() {
			list.removeChild(list.lastElementChild);
		}

		/**
		 * Создаёт список значений. Если указаны 2 и 3 аргумента типа number,
		 * то будет сгенерирован список из промежутка (аргументы 2 и 3 входят в промежуток)
		 * введённых чисел.
		 * Если вторым аргументом введён массив, то будет сгенерирован список
		 * состоящий из знасений массиав.
		 * @param {string} classItem название класса каждого элемента списка
		 * @return {boolean} если список сгенерировался возвращает true.
		 */
		this.createList = function(classItem) {
			//Проверка аргументов.
			if (typeof classItem != "string") {
				return false;
			}
			if (typeof arguments[1] == "number" &&
				typeof arguments[2] == "number"
				) {
				var startIndex = arguments[1];
				var endIndex = arguments[2];
			} else if (
				Array.isArray(arguments[1]) &&
				typeof arguments[2] == "undefined"
				) {
				startIndex = 0;
				endIndex = arguments[1].length - 1;
			} else {
				return false;
			}

			//Определяет, как должен изменяться индекс
			if (startIndex < endIndex) {
				var alter = function(number) {
					return startIndex++;
				};
				endIndex++;
			} else {
				var alter = function(number) {
					return startIndex--;
				};
				endIndex--;
			}
			while (startIndex != endIndex) {
				if (Array.isArray(arguments[1])) {
					this.addItemEndList(classItem, arguments[1][alter()]).onclick = alterValue;
				} else {
					this.addItemEndList(classItem, alter()).onclick = alterValue;
				}
			}
		};

		/**
		 * Возвращает выбранное знаение пользователем из списка значений.
		 * @return {string} выбранное знаение пользователем из списка значений.
		 */
		this.getValue = function() {
			return value;
		};

		/**
		 * Возвращает лист значений
		 * @return {HTML} лист значений
		 */
		this.getList = function() {
			return list;
		}

		/**
		 * Изменяет value на дефултное значение.
		 */
		this.setDefaultValue = function() {
			value = list.firstElementChild;
		}

		/**
		 * Открытие и закрытие списка.
		 */
		select.onclick = function(event) {
			select.lastElementChild.classList.toggle("js-select__list_hidden");
		};
	}

	/**
	 * Функция меняет количество дней в зависемости от выбранного месяца.
	 */
	function dayDependMonth() {
		for (var i = 0; i < months.length; i++) {
			if (months[i] == this.innerHTML) {
				var pickMonth = i + 1;
				break;
			}
		}
		var days = +day.getList().lastElementChild.innerHTML;
		if (pickMonth == 2) {
			for (var i = days - 28; i > 0; i--) {
				day.deleteItemEndList();
			}
		} else if (pickMonth%2 == 0) {
			if (days < 30) {
				while (days < 30) {
					day.addItemEndList("select__item", ++days);
				}
			} else {
				while (days > 30) {
					day.deleteItemEndList("select__item", --days);
				}
			}
		} else {
			while (days < 31) {
				day.addItemEndList("select__item", ++days);
			}
		}
	}

})();